window.addEventListener("load", _ => {
  const noticeBox = document.createElement("div");
  noticeBox.classList.add("notice--warning");
  noticeBox.innerHTML =
    'This new site is work in progress, for the live version go to <a href="https://bananahackers.net">bananahackers.net</a>';

  const page = document.getElementsByClassName("page__inner-wrap")[0];
  if (page) {
    page.prepend(noticeBox);
  }
});
