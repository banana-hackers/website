# [Website](http://next.bananahackers.net/)

This page is generated using jekyll. learn to use it https://jekyllrb.com/
The current theme is minimal-mistakes, its documentation is found under https://mmistakes.github.io/minimal-mistakes/docs/

### Run and Build the site locally

After [installing Jekyll](https://jekyllrb.com/docs/installation/) just run `jekyll build`
in directory of the repo which you have downloaded. Generated files are in `_site/`.