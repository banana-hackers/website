---
title: Alcatel 4044 series
---

The Alcatel OneTouch 4044 series was launched in the United States on July 31, 2017, with KaiOS 1.0 (the first stock version) and uses Gecko 37, so there are no newer APIs (many apps won't work well) and they come without the KaiStore.

There are many variations for this device and the main difference between them depends on the manufacturer, and whether some modifications have been introduced to prevent the SIM unlocking. 
For example:

1.Alcatel OneTouch 40440O and 4044T are enabled to use ADB and DevTools. Just dial * # * # 33284 # * # * to enable the debugging USB;
2. Some versions of this device (4044C and 4044N), are not debug-enabled.

These are just a few variations of the same phone. Alcatel Cingular Flip 2 (OT-4044O, with the AT&T logo) and all the other Alcatel Go Flip models are basically the same device, released under different operators.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SJv7DJ7kq84" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### EDL Mode
Being Qualcomm-based KaiOS devices, to access EDL mode:

1.Reinsert battery but don't turn the device on;
2.While holding both volume buttons, insert the USB cable;
3.Hold the buttons until white screen, and then "Entering downdload mode" appears, then release Volume Down;
4.Keep holding Volume Up until the black screen.

We have a firehose loaders that should be compatible with all the 4044 serie (tested on OneTouch-4044O and 4044T). You can also download an EDL tool adapted specifically for this device (on GitHub) and some Firmware files are available on the internet.

- Loader 0x009600e100420024 [Download](http://edl.bananahackers.net/loaders/0x009600e100420024.mbn)
- Other loaders for Alcatel [Download](https://cloud.disroot.org/s/EZDzeRq5FgS6CJZ/download)
- andybalholm/edl.py for Alcatel [Github](https://github.com/andybalholm/edl)
- Stock firmware for 4044N on [firmwarefile.com](https://firmwarefile.com/alcatel-go-flip-4044n)











