---
title: Mediatek-based Devices
---

All devices with Mediatek chipsets able to get the Developer menu in Settings using the "cache injection" method will be added on this page. Note that ADB will most likely not be authorized on your device, so you need to rely on other tools to patch the /boot partition (unsafe):
1. Use SP Flash Tool to get two backup-copies of your boot partition [SP FLASH TOOL](https://spflashtool.com/) keep one to prevent problems, and edit the other;
2. Use MTK-Tools by Bruno Martins (on GithHub) to easily unpack / repack boot.img; [MTK-TOOLS by Bruno Martins](https://github.com/bgcngm/mtk-tools).
3. Use a text editor and change the following values in the "default.prop" file:

- ro.debuggable must be 1 (in this way the device is able to use debug);
- ro.adb.secure must be 0 (needed to enable ADB).

4. Use SP Flash Tool again to flash the boot partition. Now you're ready to use ADB normally using the jailbreak method for Mediatek-based KaiOS devices:[Mediatek Jailbreak](https://sites.google.com/view/bananahackers/install-omnisd/mediatek-jailbreak).


<figure>
	<img src="https://i.postimg.cc/pr9dKwGL/mediatek.png" />
</figure>



### Indosat Ooredoo Hape Online
Thanks to Mus Tofa from [Discord](https://discord.com/invite/rQ93zEu), we also know working codes that should work on other MTK devices:


- i)Deacativate call barring: *#33# call button
- ii)Deactivate call waiting: *#43# call button
- iii)Check IMEI: *#06#
- iv)SAR Info: *#07#
- v)MTK Logger: *#*#0574#*#*
- vi)Enginering mode: *#364# with call button, or *#7788#, or *#*#2637643#*#* with call button, or *#*#3646633#*#* with call button, or *#8378269#
- vii)MMI Test: *#2886#, or *#5566#, or *#8888# with call button, or *#8301#
- viii)Hardware info: *#68140#, or *#8802# with call button
- ix)Factory reset: *#5701#
- x)SW Version: *#8808# with call button, or *#*#1212#
- xi)Aging Test: *#8816#
- xii)Version: *#87# with call button, or *#29864#* with call button.
- [take a look to cain.bananahackers.net](https://cain.bananahackers.net/)


### Jazz Digit 4G

On this KaiOS device sold in Pakistan, you can use the "cache injection" method to activate the developer menu in "Settings".
[Source](https://groups.google.com/d/msg/bananahackers/poAjhuSrRcM/dFxn2IRYAAAJ) and [take a look to cain.bananahackers.net](https://cain.bananahackers.net/).


### Sigma X-Style S3500 sKai

The first exclusive Ukrainian KaiOS phone, based upon MediaTek MT6572, runs an heavily customized KaiOS 2.5.1.1 version. The recovery menu shows up the KOT49H marking, which means it's based upon Android 4.4.2 low-level base, and release-keys. Recovery, which is entered by powering up while holding #, shows a way of custom userdata backup/restore system. Among known working codes are *#06#, *#07# (SAR level, shows 0), *#2886# (KaiOS MMI test), *#8378269# (cut-down MTK Engineering menu) and *#*#0574#*#* (MTKLogger). The "app:// URL" trick has been fixed, so we can't view the app asset contents from browser.
[Source](https://groups.google.com/forum/#!topic/bananahackers/DrA8CH8L7Yk).
[take a look to cain.bananahackers.net](https://cain.bananahackers.net/)











