---
title: QUALCOMM-BASED
---

Qualcomm-based KaiOS devices should all work with a single code, * # * # 33284 # * # *, but really only for Nokia 8110 4G, Nokia 2720 Flip and Nokia 800 Tough (all sold all over the world except in North America) and on a couple of very early [Alcatel OT-4044x models](https://next.bananahackers.net/docs/devices/alcatel-4044) with KaiOS 1.0 (exactly 4044O and 4044T) this rule can be applied.

The two Alcatel just mentioned were just the first US prototypes put on the market prematurely, that is, before all the US manufacturers of Alcatel devices with KaiOS began to disable the debug mode on them.

All Qualcomm-based devices sold exclusively in the US (Alcatel OT-4044x, Alcatel A405DL, the new Alcatel 4052x and Doro 7050) or Europe (CAT B35, Doro 7060, Maxcom MK241) by default do not have debug mode. Some only have ADB acces, but useless without debugging.

All devices with Qualcomm chipsets that can sideload apps using the "manual jailbreak" or the "cache injection" method will be added on this page.


## Qualcomm-based KaiOS Phones

Technically the devices listed here are not the most suitable among KaiOS phones to develop applications, but surely it can be fun to experiment with flashing partitions. Of course, a full backup of the device is always recommended before starting extreme experiments.

<figure>
	<img src="https://i.postimg.cc/rs6HGxp5/alcatel405.png" />
</figure>

It was the first device to include the KaiStore, and for a long time it was a locked device, until we found a solution:

-  Download the firehose for CAT B35: [Download](http://edl.bananahackers.net/loaders/0x000940e100000000.mbn)

- Use the EDL.PY tool by Bkerler (GitHub): [Qualcomm Sahara / Firehose Attack Client / Diag Tools](https://github.com/bkerler/edl)

Thanks to LiKiWii U (on Discord) for this idea, and [Yossi Yehezkel](https://groups.google.com/forum/#!topic/bananahackers/91D-RDgZpmM) on Google Groups for its experiments. United States now has a newer firmware version on wich they can develop applications for KaiOS!

- [General Thread: firehose for A405DL](https://groups.google.com/forum/#!topic/bananahackers/91D-RDgZpmM)


<figure>
	<img src="https://i.postimg.cc/zGj39kZX/catb35.png" />
</figure>

Released in September 2018, CAT B35 is an almost impenetrable device. There are no secret codes useful for debugging, but given the successful experiments and tests carried out by our users, it was possible to start studying and discovering the EDL mode, accessible by holding down the two " * " and " Power " buttons and for this phone we have a firehose loader, which will allow us to "normalize" it. You can download it from here:

-  Download the firehose for CAT B35: [Download Firehose 0x000940e100000000](http://edl.bananahackers.net/loaders/0x000940e100000000.mbn)


<figure>
	<img src="https://i.postimg.cc/wBQ54NFh/jiophoneqiual.png" />
</figure>

Here are listed the nine Qualcomm-based variations of Jio Phone (2017), connected to the best references of the DuckDuckGo search engine, trying to find the more recent firehose available:

[F30C](https://duckduckgo.com/?q=F30C+lyf&atb=v199-4__&ia=web) - [F90M](https://duckduckgo.com/?q=F90M+lyf&atb=v199-4__&ia=web) - [F120B](https://duckduckgo.com/?q=F120B+lyf&atb=v199-4__&ia=web) - [F50Y](https://duckduckgo.com/?q=F50Y+lyf&atb=v199-4__&ia=web) - [F250Y](https://duckduckgo.com/?q=F250Y+lyf&atb=v199-4__&ia=web) - [F220B](https://duckduckgo.com/?q=F220B+lyf&atb=v199-4__&ia=web) - [F10Q](https://duckduckgo.com/?q=F10Q+lyf&atb=v199-4__&ia=web) - [LF2403](https://duckduckgo.com/?q=LF-2403+lyf&atb=v199-4__&ia=web) - [LF2403N](https://duckduckgo.com/?q=LF-2403N&atb=v199-4__&ia=web)

Find out more about JiPhone on the dedicated page: [Jio Phone](https://sites.google.com/view/bananahackers/devices/jio-phone)

Other debug-enabled qualcomm phones [Debug-Enabled](https://sites.google.com/view/bananahackers/devices/debug-enabled)


## Jailbreak and root on debug-locked phones


-  Learn more about the EDL tool and commands to the dump of the userdata partition, the following guide has been written taking CAT B35 as an example: [EDL TOOL](https://sites.google.com/view/bananahackers/development/edl)
 
-  The best way to patch the image of the data partition is to enable ADB and Dev Tools using this guide: [Enable ADB & DevTools](https://sites.google.com/view/bananahackers/install-omnisd/enable-adb-devtools)

  Also the cache injection method works for them: [Cache Injection](https://sites.google.com/view/bananahackers/install-omnisd/cache-injection)
  This should be enough to get ADB and WebIDE working as them should, so you will be able to perform the sideload and the jailbreak normally, like every other Debug-enabled device!

 -  If the above procedure is not enough because "permissions are denied" on ADB, you can choose to flash the boot partition (UNSAFE) by enabling ADB support using this guide: [Custom Firmware](https://sites.google.com/view/bananahackers/root/custom-firmware)

Remember to keep a clean copy of the /boot partition, in case of notification of the availability of new updates you will have to replace the partition again, but usually points 1 and 2 are sufficient to obtain the privileged mode always, also after the update, without any problem!