---
title: Doro 7060 / 7050 / 7070
---


Three different variants: Doro 7060 (DFC-0190) in Europe, 7070 (DFC-0190) in Scandinavia and 7050 (DFC-0180) in the USA. The latter is without the KaiStore. This were one of the most difficult devices to hack. Below is everything we know about this damn device, the worst among all KaiOS phones:



### How to jailbreak on Doro-branded KaiOS devices

##### Step 1: Enter in FFBM Mode

FFBM, or "Fast Factory Boot Mode" is the only way to gain unauthorized ADB access on these devices, but without USB debugging or root permissions:

- Turn off your Doro and hold Power + Volume Down simultaneously to boot, on the screen The Doro logo appears with the words "Powered by KaiOS" and a small green line with the words "FFBM Mode";

- Connect the phone to the PC using an USB cable;

- Boot the phone using the ADB command: adb shell start b2g

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GDHJH9sKTEI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Compared to normal startup, FFBM is much slower. If you unplug the cable, your device will shut down showing the low battery indicator, but nothing is true, don't worry. Everything you can do with a normal adb shell is described in the locked devices page, a category to which the Doro no longer belongs.


##### Step 2: Enable the Developer Menu using the browser

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/p-LC7ZIeAVw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Connect the browser of your phone to [https://w2d.bananahackers.net/](https://w2d.bananahackers.net/) to open the Developer Menu from any KaiOS phone, or use [https://dorime.surge.sh/](https://dorime.surge.sh/) and click on the "DORIME 2" button, this will open the hidden Developer Menu.

Either way, select "Debugger" and enable "ADB and DevTools", a bug icon should appear in the system tray above.

Debug mode will remain until you turn it off from the Developer menu or after a factory reset.

This new method, born on Discord from an idea of ​​Luxferre and Tbrrss, consists of a function that communicates via WebAPI directly with the Settings app locally, and is ideal for temporarily bringing up the Developer menu on any KaiOS phone.

<figure>
	<img src="https://i.postimg.cc/wxFBNVVp/dorime.png" />
</figure>

It also works in normal mode, but on Doro-branded devices is totally useless without those permissions only available in FFBM.

##### Step 3: perform the Jailbreak

To permanently activate the Developer menu, you can use both the [classic Jailbreak method with OmniSD](https://next.bananahackers.net/docs/install-omnisd/) (pressing the # key for the "Privileged Factory Reset"), and the new [Cache Injection method using Wallace Toolbox](https://sites.google.com/view/bananahackers/install-omnisd/cache-injection)(pressing the # key, does not require a factory reset).

<figure>
	<img src="https://i.postimg.cc/fTGQSt6s/dorojb.jpg" />
</figure>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/zYjYnWRHeNo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

##### Warnings and contraindications

- The installation of applications through WebIDE is only possible in FFBM mode, while OmniSD and bHacker app install applications normally other apps also in normal mode;

- It is not possible to use ADB except in FFBM. In normal mode only the "adb devices" command works;

- It is not possible to use any application to get root, even using OmniBB (busybox is absent on the Doro). The Doro's ADBD binary (in the boot partition) uses permissions that are still too superficial to allow the user to make changes to the system and various customizations.

### Other Curiosities about the Doro

##### Boot Modes

- Recovery Mode (Power+Volume Up), secured;

- EDL (Power+Volume Up & Down combo), no firehoses available;

- Fastboot (Power+ *), not much commands work;

- FFBM (Power+Volume Down).


##### Secret Codes

- [* #06# ]- IMEI
- [* #07#] - SAR
- [* #2100#] - test number for Doro's emergency response service
- [* #* #664# * # *] - MMI Test app
- [* # * #258# * # *] - firmware build number
- [* #18375# ]- additional version info
- [* #235543#] - LAC and Cell ID
- [* # * #76389273# * # * (* # * #SOFTWARE# * # *)] - firmware version
- [* #610000# *] - product information app
- [* #13646633#] - EngMode app
- [* #787464# ]- turn STR function on/off
- [* # * #0704# * # *] - factory reset (asks for confirmation)
- [* # * #0574# * # * ]- LogManager app
- [* #73776673# ]= toggles debug for the "Doro's emergency response service"
- [* #34247678#] = toggles diag-mode for USB

###### Disabled Codes 

- [* #0606#] - MEID
- [* #8378269# (* #TESTBOX#) ]- should open engmode activity
- [* # * #2637643# * # *] - same as TESTBOX
- [* # * #33284# * # * (* # * #DEBUG# * # *) ]- debug code, it wouldn't work anyway

[Just click here for other useless codes "incl. themes & languages"](https://groups.google.com/d/msg/bananahackers/ABLbPYa8Nho/DmI2jloKCgAJ)


### How to use xpcshell

xpcshell is an XPConnect-enabled JavaScript Shell. It is a console application that lets you run JavaScript code. Unlike the ordinary JS shell (js), xpcshell lets the scripts running in it access XPCOM functionality.

 - Turn on your Doro in FFBM (Power + Volume Down) run the "b2g" process:

```
adb shell start b2g
```

- Once the phone have finished the boot, run the following commands:

```
adb pull /system/b2g

cd/b2g

adb push libnss3.so run-mozilla.sh xpcshell /data/local/tmp

adb shell

cd /data/local/tmp

chmod 0777 ./libnss3.so ./run-mozilla.sh ./xpcshell

LD_LIBRARY_PATH=/system/b2g ./xpcshell

```

- Now you can launch commands in javascript.

[developer.moziila.org/xpcshell](https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Language_bindings/XPConnect/xpcshell)

### Conclusions: why you shouldn't buy the Doro

The Doro is a device geared for elderly people, yes, to lead them to suicide ahead of time. No KaiOS device has such a different system ROM, heavily customized by the manufacturer, and totally bad:

- it is not possible to change the arrangement of the icons in the menu;

- it is not possible to set a ringtone other than the stock ones (all horrible);

- the applications are grouped into submenus, in order to complicate your life;

- if you start the music player, you cannot close the flip otherwise the app closes in the background, and you have to reopen it from the Multimedia apps submenu;

- if you write a message, the notifications cover the text field;

- the settings menu has 5 different themes, that have in common that they all suck.

In the end, you cannot get root privileges to remedy all this crap (for now), but on the other hand jailbreak has been made possible (September 2020), yes, after two years from the official release on the market (September 2018) of this device that I myself am ashamed to call it such!

Just take a Nokia 2720 Flip, it's a thousand times better!