---
title: SPREADTRUM-BASED
---

Normally, Spreadtrum-based KaiOS devices are much more reliable in terms of software development.

However, they also have their black sheep in the family:

- definitely BLU Zoey Smart, Jio Phone and Jio Phone 2 because they had no codes;

- of Accent Nubia 50K I only found and verified the firmware files, but I don't know anyone who owns this device.

Precisely for this reason I do not want to mark all Spreadtrum devices for which we have no researchers (Africell Afriphone, iPRO Geniphone A22, Multilaser ZAPP, TECNO T901 and Zantel Smarta) as suitable devices for those who want to develop applications.

All the debug-locked devices with Spreadtrum chipset with at least the available firmware files will be added on this page, while all the other models enabled for easier debugging will be listed on the page dedicated to them.




### Spreadtrum Based KaiOS Phones

Technically the devices listed here are not the most suitable among KaiOS phones to develop applications, but surely it can be fun to experiment with flashing partitions. Once you get the firmware files for a Spreadtrum device, simply use tools to flash them.

<figure>
	<img src="https://i.postimg.cc/NG7pjsqQ/bleinubia.png" />
</figure>

##### Firmware files for Accent Nubia 50K are available on:

- [romfull.com](https://romfull.com/accent-nubia-50k-firmware-rom-flash-file)

- [naijarom.com](https://naijarom.com/accent-nubia-50k)

##### Firmware files for Blu Zoey Smart are available on:

- [firmwarefile.com](https://firmwarefile.com/blu-zoey-smart-z230l)

- [forum.xda-developers.com](https://forum.xda-developers.com/android/development/aio-blu-zoey-smart-3g-development-t4062779)


<figure>
	<img src="https://i.postimg.cc/RVSG3k4K/jiophone12.png" />
</figure>

##### Jio Phone Lyf (Spreadtrum) and Jio Phone 2

Here are listed the Spreadtrum-based variations of Jio Phone (2017) and Jio Phone 2 (2018), connected to the best references of the DuckDuckGo search engine, trying to find the more recent firehose available:
[F101K](https://duckduckgo.com/?q=F101K+lyf&atb=v199-4__&ia=web) - [F61F](https://duckduckgo.com/?q=F61F&atb=v199-4__&ia=web) - [F81E](https://duckduckgo.com/?q=F81E+lyf&atb=v199-4__&ia=web) - [F41T](https://duckduckgo.com/?q=F41T+lyf&atb=v199-4__&ia=web) - [F211S](https://duckduckgo.com/?q=F211S+lyf&atb=v199-4__&ia=web) - [LF2401](https://duckduckgo.com/?q=LF-2401+lyf&atb=v199-4__&ia=web) - [LF2402](https://duckduckgo.com/?q=LF-2402+lyf&atb=v199-4__&ia=web) - [F221S](https://duckduckgo.com/?q=F221S+lyf&atb=v199-4__&ia=web) - [F271i](https://duckduckgo.com/?q=F271I+lyf&atb=v199-4__&ia=web) - [F300B](https://duckduckgo.com/?q=F300B+lyf&atb=v199-4__&ia=web)

Find out more about Jio Phone on the dedicated page:

- [Jio Phone](https://next.bananahackers.net/docs/devices/jiophone) 

##### other debug-enabled Spreadtrum phones

Normally, on Spreadtrum-based KaiOS phones just dial the debug code * # * # 33284# * # * and a bug icon should appear in the task bar above, and also add the code * # * #0574# * # * to enable USB debugging from a menu, this allows you to use ADB and DevTools.

All devices where this rule has been verified work perfectly, and most likely every device with this chipset should work equally and without problems. Unlike many Qualcomm-based models, they are much more reliable in terms of software development. The list is really long in comparison:

- [Debug-Enabled](https://next.bananahackers.net/docs/devices/other-debug-enabled)

### Jailbreak and root on debug-locked phones

1. You can download SPD Flash Tool (for Windows) here:

- [SPD FLASH TOOL](https://spdflashtool.com/download/spd-flash-tool-r19-0-0001)

2. The best way to patch the image of the data partition is to enable ADB and Dev Tools using this guide:

- [Enable ADB & DevTools](https://next.bananahackers.net/docs/install-omnisd/enable-adb-devtools)

This should be enough to get ADB and WebIDE working as them should, so you will be able to perform the sideload and the jailbreak normally, like every other Debug-enabled device!

3. If the above procedure is not enough because "permissions are denied" on ADB, you can choose to flash the boot partition by enabling ADB support using this guide:

- [Custom Firmware](https://sites.google.com/view/bananahackers/root/custom-firmware)

Remember to keep a clean copy of the /boot partition, in case of notification of the availability of new updates you will have to replace the partition again, but usually points 1 and 2 are sufficient to obtain the privileged mode always, also after the update, without any problem!