---
title: DEBUG-ENABLED
---

Debug-enabled devices are the easiest to use for developing applications and studying KaiOS system behavior. The use of codes ( * # * #33284# * # * and * # * #0574# * # * ) is a feature that belongs mainly to those models powered by two chipsets, Qualcomm and Spreadtrum. Of the former, only the various Nokia released by HMD have the ability to debug, while the latter are almost all suitable for this function.

This page will list only the good debugging-enabled KaiOS phones based on Qualcomm and Spreadtrum, as required by the open source nature of the operating system. I thank the respective owners of the following devices for the feedback provided and all their researches.

Here is a brief overview of what you need to know before you start using any guide, the diagram below has been modified to represent only KaiOS devices with Qualcomm and Spreadtrum chipsets:

<figure>
	<img src="https://i.postimg.cc/7Zwvbfmf/jbflowchart.png" />
</figure>

Mediatek-based models also have their own easy way to enable USB debugging and jailbreak [see here](https://next.bananahackers.net/docs/devices/mediatek-based), but it will never be as simple as with these debug-enabled KaiOS phones.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/IVKY-XY0Hac" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

On these Devices you can run any jailbreak guide: [All Jailbreak Methods](https://next.bananahackers.net/docs/install-omnisd/)


## Qualcomm-based KaiOS Devices

On Qualcomm-based KaiOS phones just dial the debug code * # * #33284# * # * and a bug icon should appear in the task bar above, which allows you to use ADB and DevTools. (For example -:Nokia 2720 Flip, Nokia 800 Tough, Nokia 8110 4G etc.)

As we have already mentioned above, of these devices, only the various Nokia manufactured by HMD follow this rule. Indeed, all Qualcomm-based devices sold exclusively in the US (Alcatel OT-4044x, Alcatel A405DL, the new Alcatel 4052x and Doro 7050) or Europe (CAT B35, Doro 7060, Maxcom MK241) by default do not have debug mode, and such models will not be listed here, but in the category of the locked devices.

The whole website revolves around Nokiaa 8110 4g so choose your test without problems. Find out more on dedicated page: [Nokia 8110 4G](https://next.bananahackers.net/docs/devices/banana-phone)

These two very first models of KaiOS devices sold in the United States (For eg:- Alcatel OT-40440 and OT-4044T) are an exception among other Qualcomm-based devices, as they were the first to be tested on the market. Any other device sold in the US would later be "locked for debugging". There are many other models in this series, of which the manufacturers have already taken steps to disable the Developer options.

Find out more about Alcatel OT-4044x on the dedicated page: [Alcatel OT-4044](https://next.bananahackers.net/docs/devices/alcatel-4044)

other debug-unlocked Qualcomm phones [Qualcomm-based](https://next.bananahackers.net/docs/devices/qualcomm-based)

## Spreadtrum-Based KaiOS Devices

On Qualcomm-based KaiOS phones just dial the debug code *#*#33284#*#* and a bug icon should appear in the task bar above, which allows you to use ADB and DevTools.

(For example-: Alcatel 3078,3088, Energizer E241s,E241,H280s, IKU V400, MTN Smart S/T 3G, Nobby 231, Orange Sanza,Sanza XL, Positivo P70s, QMobile 4G plus, Tigo Kitochi 4G Smart and Wonder L2407 4G etc.)

As I have already mentioned above, of these devices, only the various Nokia manufactured by HMD follow this rule. Indeed, all Qualcomm-based devices sold exclusively in the US (Alcatel OT-4044x, Alcatel A405DL, the new Alcatel 4052x and Doro 7050) or Europe (CAT B35, Doro 7060, Maxcom MK241) by default do not have debug mode, and such models will not be listed here, but in the category of the locked devices.

Thanks to [Isaac Cherem](https://groups.google.com/forum/#!topic/bananahackers/s4NwYb-dSUM%5B1-25%5D) (Alcatel 3078), [Jose](https://groups.google.com/forum/#!topic/bananahackers/rr_AA78HgRg) (Alcatel 3088), Apox & Anthill from discord (Energizer E241s and family), phonexhero from discord (IKU V400), Nesbert M. & [Sadiq Adam](https://groups.google.com/forum/#!topic/bananahackers/36-BH7kVtMM) (MTN Smart 3G), Luxferre (Nobby 231), [Gtwo](https://groups.google.com/forum/#!topic/bananahackers/6fVRZHHAyUc) (Orange Sanza and family), [Alessandro de Oliveisa Faria, Alessandre Alessi & Positivo Team](https://groups.google.com/d/msg/bananahackers/QpUyUmOjEm4/bPg7P7CWBAAJ) (Positivo P70s), [Brian Kinyua](https://www.youtube.com/channel/UCw-XqOSQolPSUkRMzLiKPiQ) (Wonder L2407) and [Sam aka "Sarry"](https://groups.google.com/forum/#!topic/bananahackers/Soz_P024NJo) (Tigo Kitochi 4G Smart) for the feedback!

#### other debug-unlocked Spreadtrum phones

Even the devices based on Spreadtrum, have their black sheep in the family (definitely BLU Zoey Smart, Jio Phone and Jio Phone 2 because they had no codes, instead for Accent Nubia 50K I only found and verified the firmware files, but I don't know anyone who owns this device). Precisely for this reason I do not want to mark all Spreadtrum devices for which we have no researchers (Africell Afriphone, iPRO Geniphone A22, Multilaser ZAPP, TECNO T901 and Zantel Smarta) as suitable devices for those who want to develop applications. Find out more here: [Spreadtrum-Based](https://next.bananahackers.net/docs/devices/spreadtrum-based)


## Mediatek-based KaiOS Devices

Mediatek-based devices have no codes, but in most cases "Fastboot" mode and related commands work fine. ADB and debugging can be enabled by flashing a boot partition, while the "Developer" menu can be enabled by placing a single file in the cache partition (this last step also works on some devices with different chipsets). Learn more here: [Mediatek-based](https://next.bananahackers.net/docs/devices/mediatek-based)