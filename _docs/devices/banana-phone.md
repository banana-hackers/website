---
title: Nokia 8110 4G
---

Released on July 2018, this small device is the beginning of a revolution in the world of mobile operating systems. On the internet you will find hundreds of reviews and technical sheets, but here you will find what nobody has ever told you about the Nokia 8110 4G.

This is BananaHackers.net, we carry out extensive software searches and apply guides that no one has ever shown or explained to you on KaiOS, starting with the Nokia 8110 4G, also called "bananaphone", hence the name of our community!

<figure>
	<img src="https://i.postimg.cc/PNsBNQ9J/unnamed.jpg" />
</figure>



## Secret Codes
-  [* # * # 33284 # * # * - ( * # * # debug # * # * ) ]- enter/exit debug mode (allows to access the phone with both ADB and DevTools/WebIDE). A bug icon should appear in the taskbar when the debug mode is on.
 -  [ * # 06 # ] - display the IMEI(s).
-  [* # 0000 # ]- display firmware version and model number.
- [* # 0606 # ]- display the MEID(s) (all zeroes).
 - [# # # 2324 # (# # # adbg #)] - turn Qualcomm diagnostics port on/off.
- [* # 8378269 # ( * # testbox #) or * # * # 2637643 #* #* (* # * # android# * #* )] - Testbox utility (engineering menu).
- [* # * # 372733 # * # *  ( * # * #draped# * # *) or * # 2886 # (* # auto #)] - KaiOS MMI Test utility.
- [* #7223# (* #race#)] - internal firmware build and boot image versions.
- [* # * # 0574 # * # * ( * #* # 0lri # * # * ) ]- LogManager utility.
- [* #573564# (* #jrdlog#) ]- T2M Log, a cut LogManager interface.
- [* #091# ]- turn on auto-answer on incoming call.
- [*#092# ]- turn off auto-answer on incoming call.

## Codes that exist but don't work as expected

-  [* #1219# ]- UNKNOWN: "flag of customization is cleared by secret code".
- [* #07# ]- doesn't work (must have output SAR-related information).
-  [* # * #212018# * # * ]- doesn't work (must switch between privileged and non-privileged modes, including root shell).
-  [* #8378266# (*#testcom#)] - doesn't work (must also run Testbox utility).
- [ * #1314# ]- switches the auto.send.crash.sms property, whose purpose is still unknown.

## Boot modes
### . Recovery mode

Nokia 8110 has a recovery mode (it has no fastboot mode though as of now - it's blocked from the firmware). Recovery menu can be accessed either with adb reboot recovery command or by turning the phone on keeping Up button pressed until the menu appears. It allows you to install patches from both sideload interface and SD card with the right patch, view boot and kernel logs and wipe data and cache partitions. Learn more about custom recovery: [Recovery Mode](https://sites.google.com/view/bananahackers/root/recovery-mode)

### . EDL MODE

EDL mode, on the other hand, is a Qualcomm-specific "emergency download" mode that can be only entered with the "adb reboot edl" command. When you reboot into it, you'll see nothing but black screen, but it's normal. It's designed to be operable only with proprietary Qualcomm tools. To exit this mode, the easiest way is to plug the battery out of the phone and to put it back, and then turn the phone on normally. Press D-pad Up and Down simultaneously and power the phone on.

A firehose can be downloaded here: [HWID 0x000940e100420050](http://edl.bananahackers.net/loaders/0x000940e100420050.mbn)

Learn More about EDL tools: [Guide](https://sites.google.com/view/bananahackers/development/edl)

## Our Backup Sources

This is the Construct. It's our loading program. We can load anything we need:

##### Clean Dumps -:
- [TA-1059 v11.00.17.03](https://drive.google.com/open?id=1z3GW9-l3ZAdgyV9xi95pxDc1ehc4RsIT) 
-  [TA-1059 v12.00.17.06](https://drive.google.com/open?id=1n9zdQ5QdJ-bFg14TLWEod1oX0bvyMeao) 
-   [TA-1048 v12.00.17.06](https://drive.google.com/open?id=13HrjREkjba8fyl0l8p1HTRhTRmAAtvK8) 
-    [TA-1048 v13.00.17.01](https://drive.google.com/open?id=1qs05tHS1Epe9bdLjyVAqGBJpcup7q8xJ) 
-    [TA-1048 v16.00.17.00](https://drive.google.com/open?id=1FSqIN3GG6_ICOkkXJXc4EoDOwmgF4DAe) 
-    [TA-1048 v17.00.17.01](https://drive.google.com/open?id=1rm4f4o0Bof5FRHv9SCnAyfPWVe5CM_D2)

##### Update Packages -:
- [Update v11 to v12](https://drive.google.com/open?id=14de11XSuvARYxuLH_ftcF6nI6t-yF71o) 
- [Update v11 to v13](https://drive.google.com/open?id=1-Qe6LI7ZfuTohmYJUdNuwi1MpCUyIMWl) 
- [Update v12 to v13](https://drive.google.com/open?id=1dJs44snHOFE9D71Kd2Zev4pmspNGEVH5) 
- [Update v13 to v16](https://drive.google.com/open?id=1pLG4pOCjA8LLZ5FNAllPJ7wGjWTLJRIP)

##### Recovery Mode -: 
- [Gerda-Recovery with ADB root support, from the GerdaOS project (recommended)](https://drive.google.com/open?id=1ot9rQDTYON8mZu57YWDy52brEhK3-PGh) 
- [Philz Touch Recovery by Huy Minh Bui, a custom recovery with a nice user interface](https://mega.nz/#F!lwshHQJD!gnEsxDklFOc9lFFz5q3Hrg) 
- [Custom v13 Recovery a stock v13 recovey with test-keys](https://drive.google.com/open?id=1GnVY7WVAtNW0BXizB8dGFNgVIA3Q9yL3) 

##### Modem Partition -: 
- [Modem for Indian v10](https://drive.google.com/open?id=1A0UdBMAssqijQW4wcvWTldkt24X5IF0b) 

##### Learn More Here.. 
 - [Restore Everything](https://sites.google.com/view/bananahackers/backup/repair-everything) 
- [Temporary Root](https://sites.google.com/view/bananahackers/root/temporary-root) 
- [Recovery Mode](https://sites.google.com/view/bananahackers/root/recovery-mode) 

The original Nokia 8110 in "The Matrix" Movie, 1998
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Lweuy1X9Tcg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




## Differences with the Indian versions

The global version of Nokia 8110 4G was released between June and July 2018, while the Indian version was released in September of the same year. The most obvious reason is certainly the exclusivity that Jio has on the platform in India: the pre-installed applications, including the JioStore, are the same as Jio Phone. You can also see it from the menu interface, whose icons show the name of the application, while in the global version this is shown at the top, when we select the application.

<figure>
	<img src="https://i.postimg.cc/W1SL2Kmf/banana.png" />
</figure>



The guides on this site are compatible with all versions of Nokia 8110 4G, therefore also the Indian version. The delays in the release of this device in India make thinking of certain compromises that Jio had to find with HMD. In fact, not even the firmware version is the same as the global one:

- The global version starts from firmware v11 (June 2018) KaiOS 2.5, after a year (May 2019) with the firmware update v16 KaiOS 2.5.1 is also available, while the latest update of December 2019 brings with it the firmware v17 (waiting for KaiOS 2.5 .2);
- The Indian version has remained stuck to v10 of the firmware, certainly faster and more versatile than any Jio Phone, but more limited than the global one in terms of performance and software updates, as the operating system has remained with the KaiOS 2.5 version (as any other Jio Phone).

However, the fundamental features that made the Nokia 8110 4G the true protagonist of these pages have remained preserved, and not blurred (unlike the now obsolete Jio Phone), and first of all the possibility of debugging on the device.

## Third party updates for the Indian variant
Since the partitions are interchangeable from one device to another, it is possible to convert the Indian variant into a global one with the official KaiStore fully functional and KaiOS updated to the latest version available. Find out how to do it by reading the dedicated guide:

- [Install v17(KaiOS 2.5.1 on Indian Nokia 8110 4G](https://sites.google.com/view/bananahackers/customizations/de-jioize-8110)

The whole website revolves around the 8110 4G and then around all the other Nokia phones, which are the best KaiOS devices ... so choose your tests without any problem! To learn more, visit the dedicated pages.