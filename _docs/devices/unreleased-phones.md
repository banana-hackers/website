---
title: UNRELEASED PHONES 
---

                 !-----You will never find them!-------!
				 
Sometimes the official website of the manufacturer provides an idea and these phones really exist, but only in the form of prototypes to be presented on the stands of the main technology fairs, without the belief of being able to produce others in the future, perhaps for something better .

Sometimes to inflate the product portfolio on a site or fill blog pages with articles... as with many topics outside of technology, we easily come across fake news.

We know the shape of these KaiOS phones and some articles on the web, but they are not currently available. Maybe there is a model with that name, but it is not a KaiOS device. Everything is possible, but this is a page of KaiOS devices that you will never find on the market!

- Accent Nubia 60K

<figure>
	<img src="https://i.postimg.cc/Jzbtf6zS/accentnubia50k.png" />
</figure>

this phone is mentioned only on the official KaiosTech website, perhaps to expand the portfolio, but each search result leads to [Accent Nubia 50K](https://next.bananahackers.net/docs/devices/spreadtrum-based), of which the firmware files are available.

- Energizer P280s

<figure>
	<img src="https://i.postimg.cc/hvkPwTWS/energizerp280s.png" />
</figure>

the only reference is on the official Energizer website, but it is only a prototype that will be released perhaps towards the end of 2020. It is potentially the only true device among the other listed on this page.

- Energizer Energy E220 and E220s

<figure>
	<img src="https://i.postimg.cc/cCgLXTz9/energizere220.png" />
</figure>

this device is not commercially available, and there is no evidence on its real existence, apart from a reference on ["gsmarena.com"](https://www.gsmarena.com/compare.php3?idPhone1=9572&idPhone2=9571) and a similar prototype never turned on in [this video](https://www.youtube.com/watch?v=943cB6MV3aM).

- Energizer Hardcase H242 and H242s

<figure>
	<img src="https://i.postimg.cc/9MNfdsLw/ebnergizerh242.png" />
</figure>

this device is not commercially available, and there is no evidence on its real existence, apart from a reference on ["gsmarena.com"](https://www.gsmarena.com/compare.php3?&idPhone1=9562&idPhone2=9563).

- Orange Sanza 2

<figure>
	<img src="https://i.postimg.cc/B6v61JBG/orangesanza2.png" />
</figure>

this phone is mentioned only on the official KaiosTech website, perhaps to expand the portfolio. The release of [Orange Sanza XL](https://next.bananahackers.net/docs/devices/debug-enabled) with a 2.8-inch screen has most likely made production unnecessary.

- Telma Wi-Kif 3G +

<figure>
	<img src="https://i.postimg.cc/yNjxdD9P/telma.png" />
</figure>

this phone is mentioned on the official KaiosTech website, and it really exists, but it is not a KaiOS device, as it was released well before the conception of KaiOS. Only the 4G version is a true KaiOS device.

- WizPhone WP006 4G

<figure>
	<img src="https://i.postimg.cc/BQRK9PcP/wizphone.png" />
</figure>

this phone was supposed to be the first KaiOS device produced by Google, but the news had only filled articles with images of prototypes. Announced in December 2018, it has never been released.