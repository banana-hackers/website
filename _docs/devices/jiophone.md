---
title: Jio Phone & Jio Phone 2
---

Lyf JioPhone is a line of phones marketed by Jio, in India, which includes 19 different models running KaiOS 2.5. The first model, released in August 2017 (candybar), is divided into 18 different variants, 9 with Qualcomm chipsets and 9 with Spreadtrum. In July 2018, the company unveiled JioPhone 2, F300B (with a QWERTY keyboard and a horizontal display), with Spreadtrum chipset.

Both models include a Jio branded application store instead of KaiStore, named Jio Store, and the number of applications is limited by the company's choices, including their apps and services, unlike all the other KaiOS devices sold worldwide that instead rely on the same KaiStore in a global way. However, it was possible to sideload third-party apps, such as 
[OmniSD](https://next.bananahackers.net/docs/install-omnisd/), until the release of a new targeted OTA update in the autumn of 2019, wich disabled compatibility with extensions needed [learn more](https://groups.google.com/forum/#!msg/bananahackers/XSGHJV1p3X8/yTfsKmbGBgAJ) and preventing the compatibility with old firehose loaders used to flash a patched data partition (the only safe way not to break the updates). This constraint for all Jio users was a real declaration of war!

Fortunately, the best minds have managed to circumvent this problem, also thanks to the user base, which is the largest for KaiOS devices worldwide, making it the second most used mobile OS in India, more than Apple's iOS!

Jio Phone can be manually updated to a newer firmware version, with all the advantages included in KaiOS 2.5.1 and later, and which enables the WiFi hotspot and the debug code thanks to the porting of system ROMs from other KaiOS devices!

On this page you will find all the guidelines to have full control on your Jio Phone device, whatever your variant. More information will be introduced as new open source solutions become available. If that's not enough, I'll provide links to all the major sources (blogs and YouTube channels) if it's too difficult (or too English) to understand. OK? let's go!


### Safe Jailbreak and Root
. [F90M, F120B, F220B, LF2403N, F30C , F50Y, F250Y, F10Q and LF2403]

#### 1. Download the firmware files for your variant

Here are listed the 18 different variations of Jio Phone (2017) and Jio Phone 2, connected to the best references of the DuckDuckGo search engine, trying to find the more recent firehose available:

Qualcomm: [F30C](https://duckduckgo.com/?q=F30C+lyf&atb=v199-4__&ia=web) - [F90M](https://duckduckgo.com/?q=F90M+lyf&atb=v199-4__&ia=web) - [F120B](https://duckduckgo.com/?q=F120B+lyf&atb=v199-4__&ia=web) - [F50Y](https://duckduckgo.com/?q=F50Y+lyf&atb=v199-4__&ia=web) - [F250Y](https://duckduckgo.com/?q=F250Y+lyf&atb=v199-4__&ia=web) - [F220B](https://duckduckgo.com/?q=F220B+lyf&atb=v199-4__&ia=web) - [F10Q](https://duckduckgo.com/?q=F10Q+lyf&atb=v199-4__&ia=web) - [LF2403](https://duckduckgo.com/?q=LF-2403+lyf&atb=v199-4__&ia=web) - [LF2403N](https://duckduckgo.com/?q=LF-2403N&atb=v199-4__&ia=web)

Spreadtrum: [F101K](https://duckduckgo.com/?q=F101K+lyf&atb=v199-4__&ia=web) - [F61F](https://duckduckgo.com/?q=F61F&atb=v199-4__&ia=web) - [F81E](https://duckduckgo.com/?q=F81E+lyf&atb=v199-4__&ia=web) - [F41T](https://duckduckgo.com/?q=F41T+lyf&atb=v199-4__&ia=web) - [F211S](https://duckduckgo.com/?q=F211S+lyf&atb=v199-4__&ia=web) - [LF2401](https://duckduckgo.com/?q=LF-2401+lyf&atb=v199-4__&ia=web) - [LF2402](https://duckduckgo.com/?q=LF-2402+lyf&atb=v199-4__&ia=web) - [F221S](https://duckduckgo.com/?q=F221S+lyf&atb=v199-4__&ia=web) - [F271i](https://duckduckgo.com/?q=F271I+lyf&atb=v199-4__&ia=web) - [F300B](https://duckduckgo.com/?q=F300B+lyf&atb=v199-4__&ia=web)

Note: [for JioPhone 2 (F300B) a firehose is also availabe on edl.bananahackers.net](http://edl.bananahackers.net/loaders/uhwid_jio2.mbn)

#### 2. Use flashing tools for your chipset

Normally, along with the downloaded files, there are some tools that you can use to flash the phone.

. for Qualcomm use this multiplatform tool:

[EDL TOOLS](https://sites.google.com/view/bananahackers/development/edl)

. for Spreadtrum use this tool(for Windows):

[SPD FLASH TOOL](https://spdflashtool.com)

#### 3. Enable ADB and DevTools on "userdata"

The best way to patch the image of the data partition is to enable ADB and Dev Tools using this guide:

[MANUAL JAILBREAK](https://next.bananahackers.net/docs/install-omnisd/enable-adb-devtools)

This should be enough to get ADB and WebIDE working as them should, so you will be able to perform the sideload and the jailbreak normally, like every other Debug-enabled device!

#### 4. Enable permanent root permissions (unsafe)

If the above procedure is not enough because "permissions are denied" on ADB, you can choose to flash the boot partition by enabling ADB support using this guide:

[CUSTOM FIRMWARE](https://sites.google.com/view/bananahackers/root/custom-firmware)

Remember to keep a clean copy of the /boot partition, in case of notification of the availability of new updates you will have to replace the partition again, but usually points 1, 2 and 3 are sufficient to obtain the privileged mode always, also after the update, without any problem!


### Custom ROMs for Qualcomm

Using a custom ROM based on another Qualcomm-based KaiOS device such as Nokia 8110 4G allows Jio Phone to easily access the debug mode, for example, by typing the code * # * # 33284 # * # * , using the KaiStore and a more recent KaiOS firmware!

After a long search on a Jio Phone Lyf F90M, Sachin Borkar has released some custom ROMs from the "bananaphone" for Qualcomm-based Jio Phone models, ["GerdaOS"](https://groups.google.com/forum/#!topic/bananahackers/J2eLr8zBnJI%5B1-25%5D) and ["Nokia Stock OS"](https://groups.google.com/d/msg/bananahackers/EHLoGMQzGOU/nVn88xBGAQAJ) or "Nokia Experience". All guides, downloads and insights are available on [FactoPea.blogspot.com](https://factopea.blogspot.com)

#### GerdaOS: KaiOS 2.5 for the privacy

Forked by the original project of Luxferre (gerda.tech), it's based on Nokia 8110 4G, KaiOS 2.5, firmware 13.00.17.01, and it's a privacy-oriented custom ROM (without KaiStore or Google services), with root access by default, GerdaPKG installer and an IMEI manipulation app included: [Custom ROM and Recovery for Jio F90M](https://groups.google.com/forum/#!topic/bananahackers/J2eLr8zBnJI%5B1-25%5D)

"GerdaOS" on Lyf JioPhone F220B by [Tech with Kaios](https://www.youtube.com/c/TechwithKaiosAS)
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/S07x3z41cTo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Nokia Stock: KaiOS 2.5.1 and KaiStore

Forked from the original ROM of a Nokia 8110 4G, firmware 17.00.17.01, it's faster than GerdaOS, being a more recent KaiOS version. KaiStore, Assistant, Maps and all the apps and services you expect from a normal KaiOS phone are included. All you can do with GerdaOS can be implemented here using our apps and guides.

[Nokia Stock OS - KaiOS 2.5.1 - for Jio F90M](https://groups.google.com/d/msg/bananahackers/EHLoGMQzGOU/nVn88xBGAQAJ)


"Nokia Stock OS" on Lyf JioPhone F220B by [Tech with Kaios](https://www.youtube.com/c/TechwithKaiosAS)
<iframe width="560" height="315" src="https://www.youtube.com/embed/_fYC-09Z2eI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Custom ROMs for Spreadtrum
 . [F101K, F61F, F81E, F41T, F211S, LF2401, LF2402, F221S and F271I, maybe also F300B- Jio Phone 2]

Sachin Borkar says that some devices provided by Jio using Spreadtrum cannot be able to use ROMs from Nokia or other Qualcomm-based phones, but there are phones on our "KaiOS DEVICES" section in which the firmware is the same as those SPD based phones, then you can base future ports on that models.

Other custom ROMs for other models will be added based on your feedback on Google Groups! [Bananahackers forum](https://groups.google.com/forum/#!forum/bananahackers)

### External Links and Sources
#### Best Websites
. [All Lyf Jio Mobile Phone Boot Key- RomStage](https://romstage.com/all-lyf-jio-mobile-phone-boot-key/)
. [Factopea by Sachin Borkar](http://factopea.blogspot.com)
. [Install Hotspot in Jio Phone after New update 2020 - Tech for Us %](https://www.techforus.in/2020/02/install-hotspot-in-jio-phone.html)

#### Youtube Channels 
. [Best Tutorials](https://www.youtube.com/user/guddumishra59)
. [Custom Roms](https://www.youtube.com/c/TechwithKaiosAS)
. [Infotainment](https://www.youtube.com/channel/UCJkjMVmm0gpJ5yUyOuXMOVA)



















