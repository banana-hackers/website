---
title: Alcatel 4052R/C/W/Z
---

Alcatel Go Flip 3 (by T-Mobile) or Alcatel SMARTFLIP (by AT&T) comes with the most updated KaiOS 2.5.1, becoming the first KaiOS phone sold in the United States supporting WhatsApp, Google Assistant and more recent [APIs](https://sites.google.com/view/bananahackers/customizations/api-update). The name differ according to the telephone operator (4052R, 4052C, 4052W, 4052Z...) but they are practically always the same phone.


<figure>
	<img src="https://i.postimg.cc/VsJW7mfG/alcateli.png" />
</figure>


As it was possible to see in its predecessor [Alcatel OT-4044](https://next.bananahackers.net/docs/devices/alcatel-4044), different variants may undergo different modifications, which can more or less complicate the work for a developer, thus leading him to different approach choices.

This Qualcomm-based phone, unfortunately belongs to the category of "blocked devices", but it is only possible to enable ADB with the basic shell, using the code * # * #33284# *# * and thanks to the contribution of [dhruvkar (from Reddit)](https://www.reddit.com/r/KaiOS/comments/hav4qp/how_to_use_edl_to_enable_debug_mode_on_alcatel/.compact), it was possible to get all the secret codes:

- [* # * #33284# * # * ] - enables just adb
- [* 133 #] - Generic Failure
- [##3424# ] - pulled up a menu for diag port and serial port
- [* #33# ]- call barring (service is disabled)
- [* #43#] - call waiting (enabled for "Voice")
- [* #07#] - More Information in settings
- [* #2886#]- Gflip3 ATT MMI Test
- [* # * #28865625# * # * ] - Device Unlock code
- [* # * #825364# * # * ] - Power Save Mode
- [##4382#] - Required Monthly Test | Exercise Alert | CMSP Alert
- [* #06# ]- IMEI and SVN
- [* #2886# ]- Software Version
- [* #3228# ]- internal versions of boot, system, recover, modem, bootloader
- [* #16# ]- SAR Information
- [* # * #123321# * # * ] - DM Configuration
- [* #4636#] - System Information
- [* #7810#] - Wireless Emergency Alerts
- [##66236#] - Edit DMAcc
- [##6343#] - MEID
- [* #8378269#] - Testbox tools
- [* # * #2637643# * # *] - Testbox Tools
- [* # * #0574# * # * ] - nothing
- [* # * #3646633# * # * ] - LTE | GSM | WCDMA | Automatic
- [###232#] - Call Duration

Thanks to dhruvkar [on Reddit](https://www.reddit.com/r/KaiOS/comments/hav4qp/how_to_use_edl_to_enable_debug_mode_on_alcatel/.compact) for this list

### Difference between the various American Alcatels

Alcatel Go Flip 3 / SMARTFLIP was released on September 2019, and it is just the successful experiment of a long series of prototypes marketed under various telephone operators in the United States and which marked the birth and experimentation of KaiOS, on July 2017, with Alcatel OT-4044 and Alcatel A405DL, even before it spread to India with Jio Phone and immediately after the failure of the Mozilla's Firefox OS project, wich is the open source base of KaiOS:

- Alcatel OT-4044 (also known as Alcatel Go Flip 2 or Alcatel Cingular Flip 2) runs KaiOS 1.0.

- Alcatel A405DL (also known as Alcatel MyFlip, by Tracfone) runs KaiOS 2.5.

In some videos of the OSReview YouTube channel is possible to have a direct comparison between Firefox OS and KaiOS, in addition of course to the difference between the 1.0 and 2.5 versions of the latter:

[KaiOS 1.0 versus Firefox OS]
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5ldBq3enOCQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/kWI9P_NXUh8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
[Alcatel Go Flip (KaiOS 1.0) vs Alcatel MyFlip (KaiOS 2.5)]

Only some models of the Alcatel OT-4044 series (including Alcatel Cingular Flip 2) are enabled for debugging, while other variants of the same series are blocked just like Alcatel A405DL MyFlip and the newer SMARTFLIP / Go Flip 3.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SJv7DJ7kq84" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It therefore appears that by default all new KaiOS phones sold in the United States are always and irreparably blocked by the manufacturer, just like the Doro 7050, depriving aspiring US developers of the opportunity to participate in the experimentation and learn how the KaiOS ecosystem works.

<figure>
	<img src="https://i.postimg.cc/PrGY2DFJ/alcateli2.png" />
</figure>

To find out more about Alcatel Go / Cingular Flip 2, go to the page dedicated to the OT-4044 series: [Alcatel OT-4044](https://next.bananahackers.net/docs/devices/alcatel-4044)