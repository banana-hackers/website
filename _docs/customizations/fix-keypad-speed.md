---
title: Fix the keypad speed (safe method)
---

### By [Sylvain D](https://groups.google.com/forum/#!category-topic/bananahackers/41YHx9Q4pos)

A classic problem with the Nokia 8110 4G is the too strange typing speed. Here 
is the solution to fix it:

1. Get a copy of keyboard application's folder from the system: `adb pull /system/b2g/webapps/keyboard.gaiamobile.org`
2. Extract application.zip, open `/js/keypad.js` file and change these values: 


```
Keypad.prototype.LONGPRESS_INTERVAL=1000;
Keypad.prototype.IDLE_INTERVAL=1000;
```

to these:

```
Keypad.prototype.LONGPRESS_INTERVAL=500;
Keypad.prototype.IDLE_INTERVAL=400;
```

3. Push the app and its folder on the data partition, exactly in `/data/local/webapps` 
using [temporary root access](../root/temporary-root.md):

```
adb push keyboard.gaiamobile.org /data/local/webapps
```

4. Get webapps.json file:

```
adb pull /data/local/webapps/webapps.json
```

And change the value of the keyboard.gaiamobile.org app from this:
```
"basePath": "/system/b2g/webapps",
```
to this:
```
"basePath": "/data/local/webapps",
```

You might want to use [JSONLint](https://jsonlint.com/?code=) to verify that the format 
is correct.

5. After this change push webapps.json file in its place and reboot the phone:
```
adb push webapps.json /data/local/webapps/
adb reboot
```

After rebooting your keypad should respond in such a way to preceive one command at a time,
so without writing the same number or letter several times. All the changes which you have made
will be lost(the phone will go back to its original state) after a factory reset or update.
![Keyboard app in WebIDE](../../images/webide-keypad-thing.png)

Using WebIDE you can verify that the change is done by reading content of `/js/keypad.js` file.