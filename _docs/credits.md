---
title: CREDITS AND TARGETS
---


 - __To Luxferre, which is the founding member of this community, comp.mobile.nokia.8110, better known as BananaHackers, on August 3, 2018;__
 
- __To Ivan Alex-HC, for making community more stronger;__

- __To Sylvain BLOT, who spurred me to save Firefox OS applications from their definitive death;__

- __To Speeduploop, who taught me tricks and secrets on linux, and to whom I owe most of the guides on this website I wrote;__

- __To Sylvain D, who helps me upload and distribute applications on the B-Hackers Store, since that spring of 2019, and for his loyalty;__

- __To Perry, for his obsessive research and his creations, an example for all of us;__

- __To Vivek, aka AdvancedHACKERniV1, who is the brightest Indian I have ever known in terms of hacks, despite his young age;__

- __To mikemodder007, for his full support on Discord;__

- __To tbrrss, for his full support on community and passion for KaiOS app development;__

- __To jkelol111, for his skills with XPCShell, such as to inspire alternative tools for the sideload of applications;__

- __To saveNgo and Jashan Grover, the first Indians to inspire the guides on the root and the custom firmware, first on the Jio Phone and then for everything else;__

- __To 40min, for his support and passion towards community;__

- __To Farooq Karimi Zadeh, for the IRC channel and the Bananahackers wiki's development;__

- __To Hossain Mohammed Shoaib, for his willpower, altruism and full support;__

- __To Gluck666, for the dumps of the Nokia 8110 4G and the idea of sharing them;__

- __To Devik, for the management of the Facebook page, his loyalty and passion for this community;__

- __To all the members of the BananaHackers community, without whom all this would not be possible;__

- To YouTube users community (especially __T24 TIME__ who continue to believe in us and to support my works, the result of the work of the whole community, and which encourage me to move forward;

- To __Mozilla, for inventing B2G and then Firefox OS__, whose philosophy will always remain a source of inspiration for all of us;

- __To all the developers of the Firefox OS Marketplace__, whose applications deserve to live and grow, and for whom their work will never have been in vain;


__To you all ... thanks!__

This website is dedicated to you, a small tribute that collects and lists your projects, and directs you to sources of alternative knowledge, too vast to be contained in these few pages. I invite you to explore them all and to use them as a springboard towards innovation, because culture is a right for everyone, and not only for those who submit their data to online services and to the big giants of the online market.

__Special thanks also go to KaiosTech, for giving Firefox OS new life on these beautiful devices that we can all have.__

This website was initially designed to entice ordinary people, like us, but with a great hidden talent, to implement new functions and bring into play their possibilities to make KaiOS better, through research, tests and the necessary tools made available initially from Mozilla and then from all communities, as external support for the official development of this software.
The initial gaps are gradually being lost between the lines of time.




we will never forget the early days when we tested to get the unofficial porting of important applications such as WhatsApp, when this seemed like a utopia, so much so that this software seemed like a joke to capture the attention of users, until december 2018. For us, who always believed in the potential of Firefox OS, it was a tough battle to face, difficult to overcome, and in some respects this is a battle that is not over for many of us yet!

Today KaiOS is an operating system within everyone's reach, from the user common to developers or aspiring developers, and my attention is dedicated to the latter.
This site is used to guide users in the choice of devices, and to guide people towards the best choice.

Thanks also to HMD, which since the launch of its Nokia 8110 4G has tacitly consented to this research, paying attention to our request.

Because the sideload of third-party applications is not wrong, it belongs to the nature of Firefox OS, which is the heart that moves KaiOS.

We started this project and compiled this website on a domain that could be as accessible as possible and within my reach, with the hope that it will continue to grow and spread the basic knowledge of the tools, the awareness of your actions, the hope and the freedom of choice in the software field, and in particular on mobile devices.

We hope you know how to appreciate and support all this work that I have done for you with all my passion.

Thanks for your attention, and enjoy your stay on this site.