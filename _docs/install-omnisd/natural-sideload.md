---
title: Natural Sideload
---
## Install apps normally using ADB and DevTools
### Requirements for the installation of any appliaction on KaiOS
1. [Debug mode](/docs/development/debug-mode), your [device](/docs/devices/)
 must be able to do this. Normally it is sufficient to dial the debug code 
 `*#*#33284#*#*`, while on some devices (in addition) it is to open a menu using 
the code `*#*#0574#*#*` and from there enable the USB
 debugging.
2. [ADB (Android Debug Bridge)](/docs/development/adb), a versatile command-line
tool that allows you to communicate with a device and that facilitates a variety
of device actions.
3. [WebIDE](/docs/development/webide), the most common and complete of development
 tools. Choose an old version of Firefox (59 or earlier) or Pale Moon 
 (28.6.1 or earlier), or use the official Kaios emulator (KaiOSRT). 
Alternatively you can use other command line development tools
 like [Gdeploy](https://gitlab.com/suborg/gdeploy), that uses NodeJS, 
or [XPCshell](https://github.com/jkelol111/make-kaios-install).

### How to install applications using WebIDE
1. Enable the debug mode on your device.
2. Connect the device to the PC using a USB cable.
3. Open WebIDE and connect to the "Remote runtime" (this should work on the 
 official Kaiostr emulator), if not seen, start the 
 `adb forward tcp:6000 localfilesystem:/data/local/debugger-socket` command and 
 click again on "Remote runtime". If an error message about build date mismatch 
 appears, you can safely ignore it. If the connection doesn't work, try
 rebooting the phone, running the `adb forward` command and connecting again.
4. Select the application's folder in the "Open packaged app" button of WebIDE.
5. With the triangular "Play button" at the top center of WebIDE, the app will 
 be installed on your phone.

Related Video: https://youtu.be/SoKD7IBTvM4

### The advantage of the jailbreak through OmniSD(official method)
OmniSD allows you to get a "Privileged Factory Reset" by pressing the # key while
the app is running. The privileged mode, as the word itself says, is a boot mode
in which the user has full control over the development tools and hidden
components of the device, such as the "Developer" menu in the "Settings" app, 
you can also debug on the pre-installed applications and access the 
"Device Preferences" from WebIDE.

![WebIDE normal mode](/images/webide_normal_mode.png)
![WebIDE privileged mode](/images/webide_privileged_mode.png)

This mode still allows you to obtain updates, and can be removed simply by 
performing a normal factory reset from the Settings app or from Recovery mode. 
See more about the [official Jailbreak](/docs/install-omnisd/official-jailbreak).