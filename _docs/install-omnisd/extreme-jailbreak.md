---
title: Extreme jailbreak
---

**NOTE: installing OmniSD with this method means stopping updates permanently. This guide is for educational purposes only, I do not take any responsibility for damage to your device. Use at your own risk!**

Before the discovery of the safe method for installing OmniSD, there was **an unsafe Jailbreak method that directly patched the system partition, breaking the official updates.**

If the Recovery Mode is capable of installing zip packages, just select the [omnijb-final.zip](https://drive.google.com/open?id=1mQo8iFf4q5JdtHana56fMlQqvK9Q-GtX) file from the "Apply update from SD card" menu. This method is also known for the fact that it is the only Jailbreak method that does not require the use of a computer.

<iframe width="507" height="291" src="https://www.youtube.com/embed/TNl5pK2U3X4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Here is the content of the executable archive:**

 - **META-INF**, the folder containing flash instructions for Android executable zips;
 - **omnijb-distribution**, omnijb-distribution, the folder containing the **"application.zip"** and the **"manifest.webapp"** file of OmniSD;
 - **busybox**, **unzip** and **zip** binary files
 - *decl.patch*, text file containing the patch intended for the webapps.json file in `/system/b2g/webapps`
 - **omnirun.sh** the main installtion script containing:
 ```
 #!/tmp/busybox sh

# OmniJB package

# SMITH-omnirun by Luxferre

SETTINGSFILE='/system/b2g/defaults/settings.json'
PREFFILE='/system/b2g/defaults/pref/user.js'
OMNIJA='/system/b2g/omni.ja'
WEBAPPS='/system/b2g/webapps/webapps.json'

# mount /system and /sdcard
/tmp/busybox mount -t ext4 -o rw /dev/block/bootdevice/by-name/system /system
/tmp/busybox mount -t vfat -o rw /dev/block/mmcblk1p1 /sdcard

# create backup files on memory card
TIMESTAMP=`date +%s`
/tmp/busybox cp $SETTINGSFILE /sdcard/b2g-settings-backup.${TIMESTAMP}.json
/tmp/busybox cp $PREFFILE /sdcard/b2g-pref-backup.${TIMESTAMP}.json
/tmp/busybox cp $OMNIJA /sdcard/b2g-omni.${TIMESTAMP}.ja
/tmp/busybox cp $WEBAPPS /sdcard/b2g-webapps-backup.${TIMESTAMP}.json

# unmount /sdcard after creating the backup
/tmp/busybox umount /sdcard

# replace the developer menu setting entries
/tmp/busybox sed -i 's/"developer.menu.enabled":false/"developer.menu.enabled":true/g' $SETTINGSFILE
/tmp/busybox sed -i 's/"debug.console.enabled":false/"debug.console.enabled":true/g' $SETTINGSFILE
echo "pref('developer.menu.enabled', true);" >> $PREFFILE
echo "pref('debug.console.enabled', true);" >> $PREFFILE

# try to unpack current omni.ja version

/tmp/busybox mkdir /tmp/omnipatch
/tmp/busybox cp /system/b2g/omni.ja /tmp/omnipatch
cd /tmp/omnipatch
/tmp/unzip -q omni.ja
/tmp/busybox rm omni.ja

# patch the ImportExport module

/tmp/busybox sed -i 's/_openPackage(appFile,meta,false);/_openPackage(appFile,meta,true);isSigned=true;devMode=true;/g' modules/ImportExport.jsm

# try to repack omni.ja file

/tmp/zip -qr9XD omni.ja * > /sdcard/zip_logs.txt

# replace omni.ja with the jailbreak-patched version

/tmp/busybox cp omni.ja $OMNIJA

# place OmniSD package into system installation

/tmp/busybox cp -r /tmp/omnisd.831337.xyz /system/b2g/webapps/omnisd.831337.xyz

# patch webapps.json registry

MANCONT=$(/tmp/busybox cat $WEBAPPS)
echo -n ${MANCONT%?} > /tmp/webapps.json
/tmp/busybox cat /tmp/webapps.json /tmp/decl.patch > $WEBAPPS

# unmount /system
/tmp/busybox umount /system
echo "Changes applied"
 ```
 
 <iframe width="507" height="254" src="https://www.youtube.com/embed/QpKHR-uV9gs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 
 This method (also known as "Smith method") is not recommended, unless it is in your complete interest to stop future updates. It is widely used in India, for reasons ranging from the unavailability of a computer for many users to the voluntary blocking of updates that Jio periodically releases to guarantee the exclusivity of its services on its device.

In fact, for Jio Phone users it's often necessary to downgrade the firmware and voluntarily block updates, in order to have all the services that Jio notoriously does not want to guarantee to its users (extra applications, hotspot activation or SIM unlocking in favor of other operators through apps with root privileges).

### Related topics:

 - [Custom recovery](../root/recovery-mode)
 - [Backup](../backup/)