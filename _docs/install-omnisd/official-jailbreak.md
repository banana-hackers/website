---
title: Official jailbreak
---

**The installation of third-party applications is a natural property of KaiOS** ([source](https://developer.kaiostech.com/getting-started/env-setup/os-env-setup)). The installation of OmniSD is a classic example, useful, because it allows you to install packeged third-party apps wherever you wants, perhaps saved in the personal cloud, on the sd card to be used if necessary or to share with other people, because an official Store cannot satisfy the wishes of all people, for monetization reasons (such as root apps). The jailbreak on KaiOS:

1. enables "Developer" menu in the Device tab of Settings;
2. disables navigator.mozApps.mgmt.import method package signature checks;
3. installs OmniSD, that allows you to install KaiOS apps from /sdcard/downloads or /sdcard/apps folders.

**The safe jailbreak does not stop the updates** and allows you to do what you want in complete freedom, because **we believe that those who buy a device should be free to choose what to do with it.**

<iframe width="507" height="290" src="https://www.youtube.com/embed/IVKY-XY0Hac" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Sideload requirements

To perform the Jailbreak, as for the installation of any other application, you need:

1. That your [device](../devices/) must be able to get the [**Debug mode**](../development/debug-mode), normally it's enoug to dial one or two codes;
2. [**ADB** (Android Debug Bridge)](../development/adb), a versatile command-line tool for Android and KaiOS;
3. [**WebIDE**](../development/webide), choose between Firefox 59 (or earlier), Pale Moon 28.6.1 (or earlier) and KAIOSRT. Alternatively you can use other command line development tools like [Gdeploy](https://gitlab.com/suborg/gdeploy) (that uses NodeJS) or [XPCshell](https://github.com/jkelol111/make-kaios-install).

**NOTE: all your data will be wiped during the process, this operation is essential to start the phone in "privileged mode" and therefore activate all the hidden functions mentioned above.**

### Proceedings

1. Update the phone to the recent firmware available.
2. Dial the secret code `*#*#33284#*#*` (`*#*#debug#*#*`) with your keypad. A bug icon should appear in the system taskbar above (as already said, on some devices it is necessary to open a menu using the additional code `*#*#0574#*#*` and from there enable the USB debugging).
3. Connect the phone via ADB. If WebIDE does not see the device, run `adb forward tcp:6000 localfilesystem:/data/local/debugger-socket` from the terminal, then set WebIDE to connect to "Remote Runtime" at the localhost:6000 address. If an error message about build date mismatch appears, you can safely ignore it. If the connection doesn't work, try rebooting the phone, running adb forward command and connecting again.
4. Download and unpack the standalone OmniSD package from [here](https://groups.google.com/group/bananahackers/attach/21a2c79eb6524/OmniSD.zip?part=0.2&authuser=0) (alternative links [here](https://cloud.disroot.org/s/QTC5oM4tZ9rWpAZ) or [here](https://omnijb.831337.xyz/OmniSD.zip)). Select its folder in the "Open packaged app" of WebIDE.
5. Run OmniSD with green triangle from WebIDE.
6. If everything is fine, press # when the utility is run and confirm the prompt to run the privileged factory reset.
7. After the reset is complete, "Developer" menu should appear in the "Device" tab of Settings. Enable debugger in the "ADB and DevTools" mode or use `*#*#debug#*#*` code once again.
8. Repeat steps 3 to 5, and OmniSD will be installed on your phone and ready to accept the app packages.

<small>[source by Luxferre](http://omnijb.831337.xyz/)</small> 

#### Recommendations for Spreadtrum-based KaiOS devices

If you're using a Spreadtrum-based device and you're having troubles performing step 6, replace OmniSD with Wallace-Spreadtrum-KitKat ([here](https://groups.google.com/group/bananahackers/attach/cff5882d389fb/wallace-spreadtrum-kitkat_for-omnisd.zip?part=0.4&authuser=0)) at the step 4 (extract the internal application.zip archive to sideload it using WebIDE). More details about Wallace and temporary root apps [here](../root/temporary-root).

### App format accepted by OmniSD

OmniSD accepts app packages in the .zip format with a specific structure. An archive must contain three files:

1. **application.zip** file with the actual WebIDE-compatible KaiOS/FFOS app
2. **update.webapp** file which can be empty but must be present
3. **metadata.json** file in the following format:
```
{"version": 1, "manifestURL": "app://[your_app_id]/manifest.webapp"},
```

where `[your_app_id]` has to be replaced with the actual ID (origin) of your app registered in the manifest, and manifest.webapp has to be renamed if it's called differently in the application.zip archive.

Other than that, the application structure in application.zip must match the general KaiOS and Firefox OS app structure.

**More details on app development and related sources in the dedicated guide: [Your app for OmniSD](../development/your-first-app)**