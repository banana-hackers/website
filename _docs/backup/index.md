---
title: Backup
---

[Backup / Restore your KaiOS phone - all YouTube videos](https://www.youtube.com/playlist?list=PLrmJqZiRHC9ObK5mmthIBTFled-wrSz9Q)

In this section you will find useful tools and guides that will orient you between the exact partitions and the locations to save in case of problems with your KaiOS (or Android) device. A backup can be offered in two ways, depending on your needs:

 - Take only the files you need, so that if your device is formatted, you can get your configurations back, including messages, contacts and saved media;
 - Copy one or more partitions. This takes up more space on the SD card or on the computer. Typically this method is required if you want to format entire partitions.
 
Now that we have [temporary root apps like Wallace and ADBroot](../root/), running them we can easily extract or restore everything simply using a command from the shell.

 - [Only data and settings](data-and-configurations)
 - [Dump all partitions](dump-all-partitions)
 - [List of partitions](list-of-partitions)
 - [Repair everything](repair-everything)