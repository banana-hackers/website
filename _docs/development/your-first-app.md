---
title: Your first app
---

In this page I will try to explain in an extremely basic form the functions of the 
components of an application for KaiOS / Firefox OS:

 - **KaiOS is based on Firefox OS** and as such **it supports applications in HTML, 
JavaScript, and CSS. Just like the current webpages** that we have get used to using on a daily 
basis. We can see in certain respects KaiOS as a browser transformed into an operating system.

 - The name **Boot2Gecko** or **B2G** suggests that in fact the engine is 
[Gekco](https://en.wikipedia.org/wiki/Gecko_(software)) used in all versions of Firefox
 prior to 57. When you open Settings app and look in the "tab" more information, you can see 
**the platform is 48.0a2**. This confirms the hypothesis that in reality it is a browser, 
**Firefox 48**, as also suggested by certain webpages that we explore, **identifying ourselves
 with the Mozilla's browser**.

Having said that we can assume that the applications we are using are nothing more than the
 tabs of the navigation of a normal browser. This also makes us understand that any website
 can become a webapp. So being able to move a website offline on your device makes it a full
 fledged webapp.

## The structure of a KaiOS app

 - The **HTML** file is the central nervous system of an application. In it all elements that
 are part of that page are connected. In other words, all elements which make
 that application move are in the main file. The application structure is started through a
 parameter, **`launch_path`** which requires an HTML file(usually named `index.html`) in the
 specified path (usually in the main folder/directory) and in which the functions and calls
 to the individual components of the application are indexed. Using this rule you know how
 to start with the structure.

 - **CSS** lists the styles and the rendering of the page and its verious HTML elements that
 will be displayed to the final user. Basically it is the GUI of the application and sometimes
 is not needed for more simple and basic applications(it can be replaced from pictures).

 - **Javascript** or **JS** is practically the musculoskeletal system of the application, and
 is used to make all elements move according to the instructions provided inside them. It really 
 is the workforce of the application.

 - The **Manifest** is the heart and lymphatic system of the application. It contains instructions
 for moving the whole structure of the "webpage". Without it the application has no "life".

## About the manifest.webapp file
### What are the permissions in manifest
The **permissions** field in the [app manifest](https://developer.mozilla.org/en-US/docs/Archive/B2G_OS/Firefox_OS_apps/Building_apps_for_Firefox_OS/Manifest)
 controls the app's access to various sensitive APIs on the device(sometimes called [Web APIs](https://developer.mozilla.org/en-US/docs/Web/API)).
 Apps based on the permissions they have are in 3 groups:
 - **Web apps**: These only have a basic level of permissions and don't have access to privileged
 or internal APIs.
 
 - **Privileged apps**: These have all permissions of web apps and more. 
[Hosted apps](https://developer.mozilla.org/en-US/docs/Archive/Mozilla/Marketplace/Options/Hosted_apps)
 can't be privileged apps and they must be [packaged apps](https://developer.mozilla.org/en-US/docs/Archive/Mozilla/Marketplace/Options/Packaged_apps).

 - **Internal (certified) apps**: These have all permissions of privileged and web apps plus more.
 Certified/internal apps can only be installed on a device by Mozilla or a device vendor;
 not third party developers.

For more information on app types, see [Types of packaged apps](https://developer.mozilla.org/en-US/docs/Archive/Mozilla/Marketplace/Options/Packaged_apps#Types_of_packaged_apps) 
(source http://developer.mozilla.org/)

### Difference between "hosted" and "offline" or "packaged app"
As we have seen, to start an offline app we have to set an HTML file in `launch_path`. 
To start an hosted app it's sufficient the `manifest.webapp`file, in wich you have to 
replace the `launch_path` line with a `start_url`, that requires the link of the website
 (**EXAMPLE1**).

That said, this idea that I suggested to you should guarantee more courage to those 
who want to start at a more advanced level for the first time, which is the compilation 
of the aforementioned files (for the OmniSD zip package format take a look to the 
**EXAMPLES 2 and 3** at the end of this page).

Related video: https://youtu.be/NlGlam9edgQ

## Best sources where you can learn about app development

 - http://www.w3schools.com
 - http://developer.kaiostech.com
 - http://developer.mozilla.org

## The app format accepted by OmniSD
OmniSD accepts app packages in the ZIP format. An archive must contain three files:
 - `application.zip`: The file with the actual WebIDE compatible KaiOS/FFOS app.
 - `update.webapp`: This file can be empty but must be present
 - `metadata.json`: This file should be in the following format:
   
```json
{
    "version": 1,
    "manifestURL": "app://[your_app_id_here]/manifest.webapp"
}
```

where `[your_app_id]` has to be replaced with the actual `ID` (origin) of your app registered
 in the manifest, and `manifest.webapp` has to be renamed if it is called differently in 
 application.zip archive.

Other than that, the application structure in application.zip must 
match the general KaiOS and Firefox OS app structure.

**See http://developer.kaiostech.com for more details.**

Related video: https://youtu.be/eTrcJh3Dt0A

Some models are ready to be downloaded and completed by you (you can find the 
links that refer to video tutorials in the description):

1. **Example 1**: **Webwrapper** or **hosted app**, simple, lightweight with cursor function
 but not a complete app. It needs to work online. Here's a [video tutorial](https://www.youtube.com/watch?v=1lc_fJROv8c)
 on how it works.

2. **Example 2**: **Shortcut**, this opens a website using the built in browser. 
It includes a Javascript file (main.js) that will be called from the index.html file, 
that is called from the "launch_path" of the manifest.webapp file. This is a basic
 example to start learning about Javascript and starting to create an offline app. 
A [video tutorial](https://www.youtube.com/watch?v=Bz2cMrpRmiY).

3. **Example 3**: **Offline**, This is a basic structure (empty enough!) to 
complete with a complete app to make it work on OmniSD. A 
[video tutorial](https://www.youtube.com/results?search_query=how+to+compile+css+js+html).
