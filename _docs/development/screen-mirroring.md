---
title: Screen mirroring <small>by Farooq Karimi Zadeh</small>
---

<link href="https://cdn.jsdelivr.net/gh/musky603/bananahworld/video.css" rel='stylesheet' type='text/css' />


KaiScr is a project released by [Farooq Karimi Zadeh on notabug.org](https://notabug.org/farooqkz/kaiscr), 
written in Python, to take screenshot 
from a KaiOS/FFOS device and to mirror the screen of your phone.

### Usage

<video src="http://far.chickenkiller.com/posts/using-kailive-and-kaiscr.webm" controls></video>

1. Plug the device in debug mode with a USB cable
2. Start the command `adb forward tcp:6000 localfilesystem:/data/local/debugger-socket`
3. Download the repository from here and unzip the archive
4. Open a terminal into the folder which scripts are in and start the command `python3 kailive.py`
to mirror the screen of your phone on the computer.

See video on [Farooq's website](http://far.chickenkiller.com/posts/using-kaiscr.html).



