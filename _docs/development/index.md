---
title: Development
---
### Get to know KaiOS and all the tools and guides to tame it!

 - [Tutorials for KaiOS development - all YouTube videos](https://www.youtube.com/playlist?list=PLrmJqZiRHC9MOyfHtlf-nL4f2c7DxotrR)
 - [What is KaiOS?](what-is-kaios)

## Requirements for app development

1. [Debug mode](debug-mode): Your [device](/docs/devices) must be debug-enabled.
2. [ADB & Fastboot](adb): A versatile command line tool for Android/KaiOS
3. [WebIDE](webide)

[How to enable the Developer menu](developer-menu): The hidden menu can be activated using an app or an ADB shell with root privileges.

## Other useful tools

 - [Screen mirroring](screen-mirroring): Scripts to take screenshot and mirror the screen of your KaiOS(or FFOS) device.
 - [EDL tools](edl): Firehose/Sahara client fork for MSM890x development.
 - [SP Flash Tool](https://www.evagabond.me/2018/01/install-use-sp-flash-tool-linux-ubuntu.html): Use SP Flash Tool in Linux/Ubuntu systems.

### Other development tools

 - [GdeploY (on Gitlab)](https://gitlab.com/suborg/gdeploy): CLI application manager for KaiOS using Node.js
 - [XPCshell (on Github)](https://github.com/jkelol111/make-kaios-install): Command line tools to install KaiOS apps.
 
 ## Resources for KaiOS developers
 
 - [W3schools.com](https://www.w3schools.com/): Learn HTML, JS and CSS from scratch
 - [Busybox.net](http://busybox.net): Linux sources for ROOT development
 - [Developer.kaiostech.com](http://developer.kaiostech.com): Official KaiOS sources portal for developers.
 - [Your first app](/docs/development/your-first-app): Basic guide provided by us, BananaHackers.