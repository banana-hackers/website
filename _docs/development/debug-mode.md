---
---
# Debug mode
Having the ability to debugging on a device allows you, in a complete way, 
through ADB, to check data and analyze processes, act in case of malfunctions, 
develop and install apps, root and customize your device **freely....as it should 
be for anyone purchasing a product!**
### Ways to get debugging tools
#### Debug codes on KaiOS
 - Dial the secert code `*#*#33284#*#*`, to simplify the memory suppose you write
 `*#*#debug#*#*` with your keypad. A bug icon should appear in the system taskbar
 above.

This won't work for all KaiOS devices, just try if it is enabled. If it is not 
enough to get debugging with ADB try to add another code, for example on some Spreadtrum
models (such as Positivo P70S) works `*#*#0574#*#*`. You should enable the voice
from a menu.

![Bug icon](/images/bug_icon.jpg)

#### Enable the debugger from Settings
 - Open the **Settings** app and follow this flow: **Device > Developer > Debugger > 
 ADB and DevTools**
After that, you should see your device on the USB DEVICES list...should!

![Developer menu](/images/developer_menu.png)

The menu exists, is the `/elements/developer.html` file hidden by default in the 
Settings app (`/system/b2g/webapps/settings.gaiamobile.org/application.zip`), by 
choice of the manufacturer of the device. Enforcing a **privileged factory reset** 
enables all debugging facilities. The apps [OmniSD](/docs/install-omnisd/) 
and Wallace by Luxferre includes a function where you can perform this special 
kind of factory reset by pressing the # key. In the `/js/app.js` of OmniSD we 
can read:

```javascript
case '#': //run the privileged factory reset
  if(navigator.mozPower && navigator.mozPower.factoryReset) {
    if(confirm('Do you really want to perform privileged factory reset? All your data will be wiped!'))
    navigator.mozPower.factoryReset('root')
  } else {
    alert('Error: factory reset capabilities not available')
  }
  break;
```
The problem would be solved by installing one of these apps on the affected 
device...but how?

#### Devices that do not allow for adb root/remount
If you have ADB access but without debugging, you can check the problem in this 
way:
```
adb shell getprop ro.secure
```

It should return 1 as output. You basically have to repack (which is complicated),
or [get a different boot.img](/docs/root/custom-firmware). The things that are blocking 
are in the `default.prop` file, which has to be set to:

 - `ro.secure=0`
 - `ro.debuggable=1`
 - `persist.service.adb.enable=1`

Unforuntately without mount/remount/root it's difficult to do this.

Related topics:
 - [Enable ADB and DevTools](/docs/install-omnisd/enable-adb-devtools)