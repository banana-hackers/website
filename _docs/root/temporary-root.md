---
title: Temporary Root
---

Starting with firmware version 16 for Nokia 8110 4G, 
offline versions of apps are available that allow you to manipulate 
the device using a shell from your PC.

These apps can easily be installed using [WebIDE](../development/webide) or 
[OmniSD](../install-omnisd). Like any app 
for KaiOS them are written in JavaScript, HTML and CSS.

All the applications in our resources will be listed in chronological order, 
starting from the least powerful (Telnetd) to the most complete (Wallace Toolbox).

Read their characteristics carefully, so that you can choose the right application 
for your KaiOS device. In fact, it is very likely that some features or even the whole 
application may not work, for reasons ranging from software features to hardware.


**NOTE: installing them is not risky for the device, but the use you can accidentally make of 
them can be risky. It's your full responsibility learn about these apps and how to use them, 
at your own risk!**
### Telnetd and ADBroot by [Speeduploop](https://www.paypal.me/speeduploop)

**Telnetd** It was the first root app for Nokia 8110 4G. It uses the `busybox telnet localhost` 
command to start the root shell because works on the telnet client-server protocol. Most of 
the guides on this website have been compiled on the presupposition that you use this kind of 
connection.

**ADBroot** is the more powerful root application for Nokia phones. It has the same function as 
the `adb root` command that we usually find in Android. To implement the ROOT access until the next 
reboot just click on the ENABLE button. To disable root access, press DISABLE. Its power comes from the 
implementation of its own adbd binary file.

Using ADBroot you don't need to write `busybox` or `busybox telnet localhost` and where is needed just 
write `exit` one time. Like Telnetd, it were initially designed to work on Nokia 8110 4G. 
Although these two applications seems to be similar, as we can see, they act differently to 
perform the same function. This idea of ​​implementing important binary files directly within the 
application served to inspire further versions that could work on KaiOS devices other than Nokia.

### Wallace Project by [Luxferre](https://groups.google.com/d/msg/bananahackers/DyONQrGrKlI/nLbouS3ABgAJ)

Wallace by Luxferre is a noir-style rooting app that gets the name from "[Niander Wallace](https://bladerunner.fandom.com/wiki/Niander_Wallace)" from
the "[Blade Runner 2049](https://www.google.it/search?q=Blade+Runner+2049&oq=Blade+Runner+2049&aqs=chrome..69i57j69i59j69i61l3&client=ubuntu&sourceid=chrome&ie=UTF-8)" movie of 2017. Just press the central D-pad key and wait until 
Niander Wallace silhouette fully comes up. Then your ADB shell is rooted until the next reboot.

There's also a "secret" feature of privileged factory reset (with confirmation prompt, of course) if 
you press # instead of central key.

<iframe width="507" height="285" src="https://www.youtube.com/embed/ITCx3EaFEJI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### Three different variants
 - The first release of this root application was tested to work on both Nokia 8110 4G (TA-1048 with v16 firmware) and Alcatel OT-044D (with KaiOS 1.0). 
**It also has the potential to work on the phones without busybox**, since it carries its own unzip binary and uses its own resource loader function. So, 
despite rooting, this is also a testbed for sandbox escaping (at least for an app with "certified" permission).

**Wallace 0.1** is the standard version with its own busybox version inside. [Download](https://groups.google.com/group/bananahackers/attach/cff5882d389fb/wallace-0.1.zip?part=0.2&authuser=0)

 - If you have issues with Wallace 0.1 regarding the new resource loader functionality, 
a lite version exists. Wallace lite uses busybox unzip and works on the devices with Busybox
like Nokia and Alcatels. [Download](https://groups.google.com/group/bananahackers/attach/cff5882d389fb/wallace-lite-0.1.zip?part=0.3&authuser=0)

 - There is also a fork for Spreadtrum based phones made possible by Anthill. For KaiSO Spreadtrum
KitKat based phones, Wallace Spreadtrum KitKat exists which is a fork of the original Wallace that
adds busybox command set as a bonus.

#### How works when it is running

General flow is as follows:

1. First fitting master extension is selected depending on the platform 
(choice is done among engmodeExtension, jrdExtension and kaiosExtension);

2. Resource loader (see below) method is called to fetch "unzip" binary 
from the app resources into /data/local/tmp/unzip;

3. With the help of selected master extension startUniversalCommand method, 
a concatenated command is run to:

 - stop adbd service
 - move the unzip binary from `/data/local/tmp/unzip` to `/sbin/unzip` nad adjust
its permissions (since `/sbin` structure is volatile till next reboot);
 - run the unzip command against the local application package to extract adbd binary to `/sbin`;
 - adjust the new /sbin/adbd permissions and start the adbd service again;
 - remove leftover folder from primary storage if it's empty.

#### How resource loader method works

This is the most interesting part in this app. Resource loader uses the fact that main storage 
[used by B2G API method `navigator.getDeviceStorage("sdcard")`] can always be referred to as 
`/storage/self/primary/` in the actual FS tree. The only issue is that in some cases 
(for instance, on 8110 v16 without SD card) the actual storage is mounted onto 
`/storage/self/primary/0` but, due to some bug, the root of B2G "sdcard" storage handle still refers 
to `/storage/self/primary/` itself, so attempts to write to the root result in DOM error. Hence, the logic
of the loader method is as follows:

1. Shape the XHR with "application/octet-stream" content type and "blob" response type to fetch the necessary resource file. If succeeded, go to the next step, if not, return operation error.
2. Retreive the storage handle with navigator.getDeviceStorage("sdcard") and try writing the resulting blob to 0/tmpbin.bin file (relative to the B2G storage root, not physical location). On success, go to the next step, otherwise return operation error.
3. With the help of selected master extension startUniversalCommand method, run the command to move the file (found with the find command within the /storage/self/primary/ tree, because its location might be either /storage/self/primary/tmpbin.bin or /storage/self/primary/0/tmpbin.bin) to the target location. Return operation success.


### Wallace Toolbox by [Luxferre](https://groups.google.com/forum/#!topic/bananahackers/qQmHvD8dcLs)

**NOTE: in many countries of the world it is illegal to modify the IMEI, in this case use this tool only to repair it! This guide is for educational purposes only! I take no responsibility in case of loss of updates or malfunctions of your device!**

As you may have guessed, Wallace uses its internal libraries to temporarily root a device. However, it is only a first experiment to test something wider, which can enrich a device with various additional functions. Even the installation of third-party applications involves the use of extensions and libraries on the system, but as has already happened for Jio Phone, these extensions can be lost through updates aimed at ensuring the total dependence of a device on the exclusive services of OEMs.

<iframe width="507" height="285" src="https://www.youtube.com/embed/m7hO7TwVVzE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

So imagine having to install an application that brings you these extensions, delegating the sideload to another application (for example Download or File Manager). Furthermore, as can be seen from the analyzes and tests carried out on some devices, there are hidden functions, such as the "Call Recorder" on KaiOS 2.5.2, which have not yet been officially released.

[Download Wallace Toolbox for OmniSD](https://groups.google.com/group/bananahackers/attach/db0fd8d13c403/wallace-toolbox-0.0.1-omnisd.zip?part=0.1&authuser=0)

Functions that if can be implemented by OEMs in the future, on the other hand are completely available to third-party researchers, such as Luxferre, who has added this and much more to his new creation, named **Wallace Toolbox**.

A capability of this application is to adapt to the type of device on which it is installed, recognizing the firmware version, the presence or lack of certain functions:

 - When some feature is unavailable on a particular phone, the utility will give you a corresponding error message, and the item will be in strikethrough line;
 - **WARNING! The items highlighted in red are the most dangerous and involve additional confirmation messages. USE AT YOUR OWN RISK!**

 
Yes, because this application also has properties that can possibly prevent future updates if they are not used intelligently. One above all is the IMEI editor.

#### All the power of a single application

Once the application has started, a screen will appear (in the image below) on which the various functions are listed, each connected to a phone button:

1. Give ADB root (Wallace Lite method);
2. Toggle call recording in KaiOS 2.5.2+ between manual, automatic and off;
3. Install OmniSD-compatible app packages (when the system is in privileged mode) using the system filepicker;
4. Override TTL values for wireless tethering;
5. Edit IMEI1 (on Nokias only);
6. Edit IMEI2 (on Nokias only);
7. Toggle browser's proxy setting;
8. Set the host and port for the browser's proxy;
9. Override the user agent;
\*. Run the good old overclocking script;
0. Toggle diagnostics port (on Qualcomms only);
\#. Launch the privileged mode factory reset procedure.

![Wallace Toolbox main menu](../images/wallace-toolbox.png)

Finally, we can consider this application as a sort of "secure patch" that adds some functions already present on GerdaOS, the first real custom rom of Nokia 8110 4G, but obviously security always depends on how you will use this application on your devices.

**I remind you that this guide is for educational purposes only and that any damage done to your device is under your full responsibility, depending on the use you want to do with it.**