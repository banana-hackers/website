---
title: Root <small>become the real master of your device!</small>
---

[Get root privileges on KaiOS - all YouTube videos](https://www.youtube.com/playlist?list=PLrmJqZiRHC9P97j45cFKB73OYmYmGIe4B)

Having root permissions on a KaiOS or Android device means being able to access all those functions that only the manufacturers of the device can access using the command line, and therefore ADB. These privileges depend on binary files present within multiple partitions. Having access to at least one of these really means a lot. You can get root as the default in the firmware (Boot partition) or perform backups and restores in case of damage (Recovery), or get both of these possibilities starting from the user space (using applications). 
**Getting root privileges is not risky for the device, but the use you make of it can be!**

 - [Temporary root](temporary-root) (safest method): Enables you to made all the modifications you need manually on all the firmware versions using our apps.
 - [Recovery mode](recovery-mode) (safe and unsafe methods): A working recovery is needed for flash update zips and repair the system in case that your phone is brocken.
 - [Custom firmware](custom-firmware) (parmanet root method): Apply a patch to the boot partition using a root ADBD binary and change the device's default settings.